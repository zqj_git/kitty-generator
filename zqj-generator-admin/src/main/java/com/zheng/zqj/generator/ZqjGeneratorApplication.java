package com.zheng.zqj.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 启动器
 * @author Louis
 * @date Nov 9, 2018
 */
@SpringBootApplication(scanBasePackages={"com.zheng"})
public class ZqjGeneratorApplication  extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ZqjGeneratorApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(ZqjGeneratorApplication.class, args);
	}
}
