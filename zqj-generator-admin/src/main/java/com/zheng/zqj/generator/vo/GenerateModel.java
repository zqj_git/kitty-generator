package com.zheng.zqj.generator.vo;

import java.util.ArrayList;
import java.util.List;

import com.zheng.zqj.dbms.vo.ConnParam;

/**
 * 代码生成数据模型
 * @author Louis
 * @date Nov 10, 2018
 */
public class GenerateModel {

	private String outPutFolderPath;
	private String basePackage;
	private ConnParam connParam;
	private List<TableModel> tableModels = new ArrayList<>();
	private String module;

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getOutPutFolderPath() {
		return outPutFolderPath;
	}
	public void setOutPutFolderPath(String outPutFolderPath) {
		this.outPutFolderPath = outPutFolderPath;
	}
	public String getBasePackage() {
		return basePackage;
	}
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}
	public ConnParam getConnParam() {
		return connParam;
	}
	public void setConnParam(ConnParam connParam) {
		this.connParam = connParam;
	}
	public List<TableModel> getTableModels() {
		return tableModels;
	}
	public void setTableModels(List<TableModel> tableModels) {
		this.tableModels = tableModels;
	}

}
